`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Personal Testing
// Engineer: Shea Gosnell
// 
// Create Date: 05.12.2017 16:07:22
// Design Name: SSD
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk,
    output [15:0] led,
    output [6:0] SSD,
    output [3:0] SSDsel
    );
    
    reg [39:0] count = 0;
    reg [6:0] Char = 0;
    reg [3:0] SSDsel_reg = 4'b1110;
	wire [6:0] nybble4;
    wire [6:0] nybble3;
    wire [6:0] nybble2;
    wire [6:0] nybble1;

    
    assign led=count[39:24];
    assign SSD=Char;
    assign SSDsel=SSDsel_reg;
    
    ssd_lut SSD3 (
		.nybble (count[39:36]),
		.character(nybble4)
	);
	
	ssd_lut SSD2 (
        .nybble (count[35:32]),
        .character(nybble3)
    );
    
    ssd_lut SSD1 (
        .nybble (count[31:28]),
        .character(nybble2)
    );
    
    ssd_lut SSD0 (
        .nybble (count[27:24]),
        .character(nybble1)
    );
    
    
    //on each positive edge of the clock, increment the counter
    always @(posedge(clk)) begin
        count <= count +1;   
    end
    

    always @(count[8] or nybble1 or nybble2 or nybble3 or nybble4) begin      
		casez(count[10:8])
			//intermediate state where the segments are all off
			3'b??0:	begin
					SSDsel_reg = 4'b1111;
					Char = 7'h7F;
				end
			3'b001:  begin
					SSDsel_reg = 4'b1110;
					Char = nybble1;
				end
			3'b011:  begin
					SSDsel_reg = 4'b1101;
					Char = nybble2;
				end
			3'b101:  begin
					SSDsel_reg = 4'b1011;
					Char = nybble3;
				end
			3'b111:  begin
					SSDsel_reg = 4'b0111;
					Char = nybble4;
				end
		endcase
    end
    
endmodule


//A look-up table to convert a number to a character for the SSD
module ssd_lut(
    input [3:0] nybble,
    output reg [6:0] character
    );
    
    always @nybble begin
        case(nybble)
            4'h0: character = ~8'h7E;
            4'h1: character = ~8'h30;
            4'h2: character = ~8'h6D;
            4'h3: character = ~8'h79;
            4'h4: character = ~8'h33;
            4'h5: character = ~8'h5B;
            4'h6: character = ~8'h5F;
            4'h7: character = ~8'h70;
            4'h8: character = ~8'h7F;
            4'h9: character = ~8'h7B;
            4'hA: character = ~8'h77;
            4'hB: character = ~8'h1F;
            4'hC: character = ~8'h4E;
            4'hD: character = ~8'h3D;
            4'hE: character = ~8'h4F;
            4'hF: character = ~8'h47;
         endcase
    end
endmodule