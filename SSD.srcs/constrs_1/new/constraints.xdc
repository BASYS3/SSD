#Supress compiler warning about these settings by setting them
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]

#Clock
set_property PACKAGE_PIN W5 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports clk]
    create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clk]
    
#16-bit LED output
set_property IOSTANDARD LVCMOS33 [get_ports led[*]]
set_property PACKAGE_PIN U16 [get_ports led[0]]
set_property PACKAGE_PIN E19 [get_ports led[1]]
set_property PACKAGE_PIN U19 [get_ports led[2]]
set_property PACKAGE_PIN V19 [get_ports led[3]]
set_property PACKAGE_PIN W18 [get_ports led[4]]
set_property PACKAGE_PIN U15 [get_ports led[5]]
set_property PACKAGE_PIN U14 [get_ports led[6]]
set_property PACKAGE_PIN V14 [get_ports led[7]]
set_property PACKAGE_PIN V13 [get_ports led[8]]
set_property PACKAGE_PIN V3  [get_ports led[9]]
set_property PACKAGE_PIN W3  [get_ports led[10]]
set_property PACKAGE_PIN U3  [get_ports led[11]]
set_property PACKAGE_PIN P3  [get_ports led[12]]
set_property PACKAGE_PIN N3  [get_ports led[13]]
set_property PACKAGE_PIN P1  [get_ports led[14]]
set_property PACKAGE_PIN L1  [get_ports led[15]]


#7 bit SSD output
set_property IOSTANDARD LVCMOS33 [get_ports SSD[*]]
set_property PACKAGE_PIN U7 [get_ports SSD[0]]
set_property PACKAGE_PIN V5 [get_ports SSD[1]]
set_property PACKAGE_PIN U5 [get_ports SSD[2]]
set_property PACKAGE_PIN V8 [get_ports SSD[3]]
set_property PACKAGE_PIN U8 [get_ports SSD[4]]
set_property PACKAGE_PIN W6 [get_ports SSD[5]]
set_property PACKAGE_PIN W7 [get_ports SSD[6]]

#4 bit SSD select output
set_property IOSTANDARD LVCMOS33 [get_ports SSDsel[*]]
set_property PACKAGE_PIN U2 [get_ports SSDsel[0]]
set_property PACKAGE_PIN U4 [get_ports SSDsel[1]]
set_property PACKAGE_PIN V4 [get_ports SSDsel[2]]
set_property PACKAGE_PIN W4 [get_ports SSDsel[3]]